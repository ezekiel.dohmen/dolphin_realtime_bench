

if [ "$(uname -r)" = "5.10.0-19-amd64" ]; then
echo "Running MX test."

# MX Long Link
declare -A CONFIG=( ["sys1"]="x2cdsrfm" ["sys2"]="x2iscex" ["rfm_adapter"]="1" ["discription"]="4Km")

# MX Short Link
#declare -A CONFIG=( ["sys1"]="x2cdsrfm" ["sys2"]="x2lsc0" ["rfm_adapter"]="0" ["discription"]="Short")


elif [ "$(uname -r)" = "4.19.0-25-amd64" ]; then
echo "Running IX test."

# IX Long Link
#declare -A CONFIG=( ["sys1"]="x2asc0" ["sys2"]="x2iscey" ["rfm_adapter"]="0" ["discription"]="4Km")

# IX Short Link
#declare -A CONFIG=( ["sys1"]="x2lsc0" ["sys2"]="x2asc0" ["rfm_adapter"]="0" ["discription"]="Short")
declare -A CONFIG=( ["sys1"]="x2lsc0" ["sys2"]="x2seih7" ["rfm_adapter"]="0" ["discription"]="Short")


else
echo "Unrecognized test system, exiting..."
exit
fi

#
# Other Test Parameters
#

num_writes=( "100" )
#num_writes=( "15" "18" "20" "23" "25" "28" "29" "30" "32" )
#num_writes=("1" "2" "3" "4" "5" "8" ) #16 us Long Link
#num_writes=("15" "35" "45" "80" "120" "140") #16 us Short link
#num_writes=( "16" "20" "22" "23" "24" "25" "26" "27" "28" "29" "30" "35") #61 us Long Link
#num_writes=("15" "35" "45" "80" "100" "120" "140" "200" "250" "300" "350" "400" "450") #61 us Short Link
#num_writes=(6)
delays=( "61" )

sleep_time_s=${1:-60} #Time to allow the test to run for
module="/opt/rtcds/rtscore/ej-build/dolphin_realtime_bench/bandwidth_test/bandwidth_test_"$(uname -r)".ko"
echo "module is : $module"

# END of Test Parameters


if [[ "module.c" -nt "bandwidth_test.ko" ]]; then
  echo module source is newer than ko, rebuilding...
  make
  cp bandwidth_test.ko $module
  sync
  sleep 3
  echo ""
  echo ""
  echo ""
fi

parse_bw() {
    IFS=':' read -ra BW_SPLIT_LINE <<< "${1}"
    #printf "BW is: %s" "${BW_SPLIT_LINE[2]}"
    res=$(printf "%s" "${BW_SPLIT_LINE[2]}" | tr -d '\\n') #new line gets turned into literals by printf
    echo -n $res
    
}

parse_max() {
    IFS=':' read -r crap min_crap max_crap mean_crap <<< "${1}"
    IFS=',' read -r max crap <<< "$max_crap"
    echo -n "$max"

}

parse_mean() {
    IFS=':' read -r crap min_crap max_crap mean_crap <<< "${1}"
    mean=$(printf "%s" $mean_crap | tr -d '\\n')
    echo -n "$mean"
}



run_benchmark() {

    num_writes_per_flush=$1
    #echo "arg is: " $2
    #printf -v udelay_each_flush '%s,' "${2}"
    udelay_each_flush=$2

    echo "Starting Benchmark with num_writes_per_flush=$num_writes_per_flush and udelay_each_flush=$udelay_each_flush"

    ssh ${CONFIG["sys1"]} sudo insmod $module ADAPTER_NUM=${CONFIG["rfm_adapter"]} NUM_WRITES_PER_FLUSH_ARR=$num_writes_per_flush UDELAY_PER_FLUSH_ARR=$udelay_each_flush &
    #ssh ${CONFIG["sys1"]} sudo insmod $module ADAPTER_NUM=${CONFIG["rfm_adapter"]} NUM_WRITES_PER_FLUSH_ARR=0 UDELAY_PER_FLUSH_ARR=$udelay_each_flush &
    ssh ${CONFIG["sys2"]} sudo insmod $module NUM_WRITES_PER_FLUSH_ARR=$num_writes_per_flush UDELAY_PER_FLUSH_ARR=$udelay_each_flush WRITE_TO_SEGMENT=1 &
    #ssh ${CONFIG["sys2"]} sudo insmod $module NUM_WRITES_PER_FLUSH_ARR=0 UDELAY_PER_FLUSH_ARR=$udelay_each_flush WRITE_TO_SEGMENT=1 &

    echo "Sleeping for " $sleep_time_s " seconds to let modules run..."
    sleep $sleep_time_s

    ssh ${CONFIG["sys1"]} sudo rmmod bandwidth_test
    ssh ${CONFIG["sys2"]} sudo rmmod bandwidth_test

    echo "${CONFIG["sys1"]}":
    ssh ${CONFIG["sys1"]} sudo dmesg | grep "Num writes per" | tail -1
    stats_line=$(ssh ${CONFIG["sys1"]} sudo dmesg | grep "mean:" | tail -1)
    echo $stats_line
    bw_line=$(ssh ${CONFIG["sys1"]} sudo dmesg | grep "Calculated bandwidth" | tail -1)
    echo $bw_line

    mean1=$(parse_mean "$stats_line")
    max1=$(parse_max "$stats_line")
    bw1=$(parse_bw "$bw_line")

    echo "${CONFIG["sys2"]}":
    ssh ${CONFIG["sys2"]} sudo dmesg |grep "Num writes per" | tail -1
    stats_line=$(ssh ${CONFIG["sys2"]} sudo dmesg |grep "mean:" | tail -1)
    echo $stats_line
    bw_line=$(ssh ${CONFIG["sys2"]} sudo dmesg | grep "Calculated bandwidth" | tail -1)
    echo $bw_line

    mean2=$(parse_mean "$stats_line")
    max2=$(parse_max "$stats_line")
    bw2=$(parse_bw "$bw_line")

    mean=$(( mean1 > mean2 ? mean1 : mean2)) #Pick largest
    max=$(( max1 > max2 ? max1 : max2))  #Pick latgest
    bw1_num=$(echo -n "$bw1" |  sed 's/[^0-9]*//g')
    bw2_num=$(echo -n "$bw2" |  sed 's/[^0-9]*//g')
    if (( bw1_num > bw2_num  )); then #Pick lowest
        bw="$bw2"
    else
        bw="$bw1"
    fi
    printf "\nParsed results: \n"
    printf  "| %-8s | %-8s | %-8s | %-8s | %-8s | %-8s |\n" "${CONFIG["discription"]}" "$udelay_each_flush" "$num_writes_per_flush" "$mean" "$max" "$bw"

}

for delay in ${delays[@]}; do
    for nw in ${num_writes[@]}; do
        run_benchmark $nw $delay
            echo ""
            echo "----- End of Test -----"
            echo ""
            echo ""
    done
done
