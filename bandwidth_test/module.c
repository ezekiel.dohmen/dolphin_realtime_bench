///	@file module.c 
///	@brief 

#include <linux/version.h>
#include <linux/kernel.h> 
#include <linux/module.h>
#include <linux/cpu.h>
#include <linux/delay.h>
#include <net/sock.h>
#include <linux/netlink.h>
#include <linux/skbuff.h>
#include <asm/tsc.h> //cpu_khz

#include <asm/uaccess.h>
#include <asm/cacheflush.h>
#include <linux/ctype.h>
#include <linux/spinlock_types.h>

#include "common_utils.h"
#include "dolphin_netlink.h"
#include "hist.h"
//#include "mstore.h"


#include "rts-cpu-isolator.h" 

#define RTS_LOG_PREFIX "dolphin_bw_bench"
#include "drv/rts-logger.h"




MODULE_DESCRIPTION( "" );
MODULE_AUTHOR( "LIGO" );
MODULE_LICENSE( "Dual BSD/GPL" );

#define NUM_RT_CORES 2
#define WRITE_THRD 0
#define READ_THRD 1

static atomic_t g_atom_should_exit[NUM_RT_CORES] = {ATOMIC_INIT(0), ATOMIC_INIT(0)}; 
static atomic_t g_atom_has_exited[NUM_RT_CORES] = {ATOMIC_INIT(0), ATOMIC_INIT(0)}; 
static int g_cores_used[NUM_RT_CORES] = {-1, -1};

//#define DOLPHIN_BUF_SZ 16777216 // 16MB
//#define MAX_DOLPHIN_INDEX 500000


#define DOLPHIN_BUF_SZ 1048576 // 1MB
#define MAX_DOLPHIN_INDEX 65536

#define NUM_SAMPLES_TO_RECORD 100000
int32_t g_data_cap[NUM_SAMPLES_TO_RECORD];

char g_num_per_flush_str[2000] = {0,};
char g_udelay_per_cycle_str[2000] = {0,};


// Start Module Parameters
//
static int ADAPTER_NUM = 0;
module_param(ADAPTER_NUM, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(ADAPTER_NUM, "The adapter we should use");

static int WRITE_TO_SEGMENT = 0;
module_param(WRITE_TO_SEGMENT, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(WRITE_TO_SEGMENT, "The MC segment we write to, we read from the other");

static int NUM_WRITES_PER_FLUSH_ARR[25] = {4,};
static int NUM_WRITES_PER_FLUSH_CNT = 1 ;
module_param_array(NUM_WRITES_PER_FLUSH_ARR, int, &NUM_WRITES_PER_FLUSH_CNT, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(NUM_WRITES_PER_FLUSH_ARR, "How many IPCs to send per cycle");

static int UDELAY_PER_FLUSH_ARR[25] = {16,};
static int UDELAY_PER_FLUSH_CNT = 1;
module_param_array(UDELAY_PER_FLUSH_ARR, int, &UDELAY_PER_FLUSH_CNT, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(UDELAY_PER_FLUSH_ARR, "How long we should delay after flushing the IPCs we just send");

static int WRITE_TO_FILE = 0;
module_param(WRITE_TO_FILE, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(WRITE_TO_FILE, "1 means we will write latency measurements out to a file");

static int WARN_TIME_THRESH_NS = -1;
module_param(WARN_TIME_THRESH_NS, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(WARN_TIME_THRESH_NS, "Cause the module to emit a warning if the write takes more than this value.");


//
// Start Module Specific defines and vars
//
#define SYSTEM_NAME_STRING_LOWER "bandwidth_test" 
#define BILLION 1000000000ull
#define CPURATE_HZ (cpu_khz * 1000) 


typedef struct ipc_t {
    uint64_t data;
    uint64_t count;
} ipc_t;

void write_data_to_file( void ) 
{
    struct file *i_fp;
    loff_t pos = 0;
    ssize_t bytes_out;
    i_fp = filp_open("/opt/rtcds/rtscore/ej-build/dolphin_realtime_bench/bandwidth_test/output.int32",O_RDWR | O_CREAT, 0666);
    if (IS_ERR(i_fp)) {
        printk(KERN_INFO "intput file open error/n");
        return;
    }

    bytes_out = kernel_write(i_fp, g_data_cap, NUM_SAMPLES_TO_RECORD*sizeof(int32_t), &pos);
    RTSLOG_INFO("Wrote %ld bytes to the file\n", bytes_out);

    filp_close(i_fp, NULL);
}



void wait_seconds(int sec)
{
    uint64_t us = sec * 1000000;
    while (us > 19999){
        udelay(19999);
        us -= 19999;
    }
    udelay(us);
}


void writer_rt_func( void )
{
    uint64_t counter = 0, idx=0, idx_cache, write_data_idx=0, flush_counter=0, last_counter=0;
    volatile ipc_t * write_addr = (volatile ipc_t *)g_write_addrs[WRITE_TO_SEGMENT];
    volatile ipc_t * read_addr = (volatile ipc_t *)g_read_addrs[WRITE_TO_SEGMENT];

    //Write Timing Vars
    uint64_t write_time_ns;
    hist_context_t * hist_ctx = NULL;
    int64_t ranges [] = { 500, 1000, 2000, 4000, 6000, 8000, 10000, 15000, 20000, 
    25000, 30000, 35000, 40000, 45000, 50000, 55000, 60000, 65000, 70000, 90000, 120000, 140000};
    hist_ctx = hist_initialize(sizeof(ranges)/sizeof(ranges[0]), ranges);
    if ( hist_ctx == NULL)
    {
        RTSLOG_INFO("hist pointer null, exiting RT thread.\n");
        atomic_set(&g_atom_has_exited[WRITE_THRD], 1);
        return;
    }


    if (write_addr == NULL || read_addr == NULL){
        // System reset command received
        RTSLOG_INFO("read_addr or write_addr is NULL, exiting RT thread.\n");
        atomic_set(&g_atom_has_exited[WRITE_THRD], 1);
        return;
    }


    RTSLOG_INFO("Writer thread is waiting 15 seconds, so the benchmark module "
    "on the other test system can be started and connect to the dolphin segment.\n");
    wait_seconds(15);

    //Wait until reader has signaled ready
    while(read_addr[idx].count == 0 && atomic_read(&g_atom_should_exit[WRITE_THRD]) == 0)
    {
        RTSLOG_INFO("Writer is waiting for reader to set ready in the shared memory...\n");
        wait_seconds(2);
        
    }
    RTSLOG_INFO("*** Done waiting for addr 0 to change, val 0x%llx ***\n", read_addr[idx].count);

    while(atomic_read(&g_atom_should_exit[WRITE_THRD]) == 0)
    {

        
        for (unsigned i = 0; i < NUM_WRITES_PER_FLUSH_ARR[flush_counter%NUM_WRITES_PER_FLUSH_CNT]; ++i)
        {
            //timer_start( &write_time_ns );
            idx_cache = idx;
            write_addr[idx].count = counter;
            //clflush_cache_range( (void*)&write_addr[idx], sizeof(ipc_t) );
            ++counter;
            idx = (idx + 1) % (MAX_DOLPHIN_INDEX);
            //write_time_ns = timer_end_ns( &write_time_ns );
            //hist_add_element(hist_ctx, write_time_ns);
        }

        //Start timer annd flush data	
        if ( counter != last_counter )
        {
            timer_start( &write_time_ns );
            clflush_cache_range( (void*)&write_addr[idx_cache], sizeof(ipc_t) );
        
            write_time_ns = timer_end_ns( &write_time_ns );
            hist_add_element(hist_ctx, write_time_ns);
            if ( write_data_idx < NUM_SAMPLES_TO_RECORD) {
                g_data_cap[write_data_idx] = (int32_t) write_time_ns;
                ++write_data_idx;
            }
            last_counter = counter;
        }

	if( WARN_TIME_THRESH_NS > 0 && write_time_ns > WARN_TIME_THRESH_NS) {
		RTSLOG_WARN("Write took %lu ns to complete\n", write_time_ns);
	}

        udelay(UDELAY_PER_FLUSH_ARR[flush_counter%UDELAY_PER_FLUSH_CNT]);
        ++flush_counter;


    }


    if( counter == 0 ) //No good stats data
        RTSLOG_RAW("min: 0, max: 0, mean: 0\n");

    RTSLOG_INFO("Writer thread got shutdown signal, counter was at %llu\n", counter);

    common_print("Histogram Of All Write Latencies (ns)\n");
    hist_print_stats(hist_ctx);
    hist_free( hist_ctx );


    char hold[25];

    for(int i=0; i < NUM_WRITES_PER_FLUSH_CNT; ++i)
    {
        snprintf(hold, 25, "%d", NUM_WRITES_PER_FLUSH_ARR[i]);
        strncat(g_num_per_flush_str, hold, 2000);
        if( i != NUM_WRITES_PER_FLUSH_CNT-1)
            strncat(g_num_per_flush_str, ",", 2000);
    }

    for(int i=0; i < UDELAY_PER_FLUSH_CNT; ++i)
    {
        snprintf(hold, 25, "%d", UDELAY_PER_FLUSH_ARR[i]);
        strncat(g_udelay_per_cycle_str, hold, 2000);
        if( i != UDELAY_PER_FLUSH_CNT-1)
            strncat(g_udelay_per_cycle_str, ",", 2000);
    }

    RTSLOG_INFO("Num writes per flush: %s - udelay per cycle: %s\n", g_num_per_flush_str, g_udelay_per_cycle_str);
    

    // System reset command received
    atomic_set(&g_atom_has_exited[WRITE_THRD], 1);
    return;
}

void reader_rt_func( void )
{
    uint64_t expected_count = 0, total_received = 0, idx=0, invalid=0;
    volatile ipc_t * read_addr = (volatile ipc_t *)g_read_addrs[1]; //Default to read from seg 1
    volatile ipc_t * write_addr = (volatile ipc_t *)g_write_addrs[1];

    uint64_t tot_start_time, tot_end_time_ns;

    if (WRITE_TO_SEGMENT == 1){
        read_addr = (volatile ipc_t *)g_read_addrs[0];
        write_addr = (volatile ipc_t *)g_write_addrs[0];
    }

    if (read_addr == NULL || write_addr == NULL){
        RTSLOG_INFO("read_addr or write_addr is NULL, exiting RT thread.\n");
        atomic_set(&g_atom_has_exited[READ_THRD], 1);
        return;
    }

    RTSLOG_INFO("Reader thread is waiting 10 seconds, so the benchmark module "
    "on the other test system can be started and connect to the dolphin segment.\n");
    wait_seconds(10);

    //RTSLOG_INFO("About to read from %llu\n", (uint64_t) read_addr);
    //RTSLOG_INFO("Value %llx\n", *read_addr);

    write_addr[0].count = 0xFFFFFFFFFFFFFFFF; //Max uint64, signal reader is ready
    clflush_cache_range( (void*)write_addr, 16 );

    
    while(atomic_read(&g_atom_should_exit[READ_THRD]) == 0)
    {

        while( read_addr[idx].count != expected_count && atomic_read(&g_atom_should_exit[READ_THRD]) == 0)
        {
            //Do nothing
            //udelay(1);
        }

        if ( read_addr[idx].count == expected_count )
        {
            ++total_received;
            ++expected_count;
            idx = (idx + 1) % (MAX_DOLPHIN_INDEX);
            if( total_received == 1) timer_start( &tot_start_time );
        }
        else 
        {
            ++invalid;
        }

    }
    tot_end_time_ns = timer_end_ns( &tot_start_time );
    RTSLOG_INFO("Reader thread got shutdown signal, received %llu counts, invalid %llu.\n", total_received, invalid);
    RTSLOG_INFO("Calculated bandwidth: %llu MB/s\n", (total_received * sizeof(ipc_t))  / (tot_end_time_ns / 1000));


    // System reset command received
    atomic_set(&g_atom_has_exited[READ_THRD], 1);
    return;
}





void stop_rt_thread(int index)
{
    uint64_t stop_sig_time_ns = get_monotonic_time_ns();
    ISOLATOR_ERROR ret;
    char error_msg[ISOLATOR_ERROR_MSG_ALLOC_LEN];

    // Stop the code and wait
    atomic_set(&g_atom_should_exit[index], 1);
    while (atomic_read(&g_atom_has_exited[index]) == 0)
    {
        msleep( 1 );
        //RTSLOG_INFO("Waiting for thread %d to exit...\n", index);
    }

    RTSLOG_INFO("It took %lld ms for the RT code to exit.\n",
                 (get_monotonic_time_ns() - stop_sig_time_ns)/1000000);

    //At this point the CPU will be down...
    ret = rts_isolator_cleanup( g_cores_used[index] );
    if( ret != ISOLATOR_OK)
    {
        isolator_lookup_error_msg(ret, error_msg);
        RTSLOG_INFO("Error : rts-cpu-isolator : There was an error when calling rts_isolator_cleanup(): %s\n", error_msg);
    }
    //RTSLOG_INFO("Done with cleanup on thread: %d\n", index);

}


// MAIN routine: Code starting point
// ****************************************************************
/// Startup function for initialization of kernel module.
int rt_init( void )
{
    ISOLATOR_ERROR ret;
    char error_msg[ISOLATOR_ERROR_MSG_ALLOC_LEN];
    unsigned segment_ids[] = {0, 1};
    unsigned adapters[] = {ADAPTER_NUM, ADAPTER_NUM};

    //Create 2 segments
    if (init_dolphin(2, DOLPHIN_BUF_SZ, segment_ids, adapters) == -1)
    {
        finish_dolphin();
        return -1;
    }

    RTSLOG_INFO("%s: Locking free CPU core for rt1\n", SYSTEM_NAME_STRING_LOWER );
    ret = rts_isolator_run( writer_rt_func, -1, &g_cores_used[WRITE_THRD]);
    if( ret != ISOLATOR_OK)
    {
        isolator_lookup_error_msg(ret, error_msg);
        RTSLOG_INFO("Error : rts-cpu-isolator : %s\n", error_msg);
	    finish_dolphin();
        return -1;
    }


    RTSLOG_INFO("%s: Locking free CPU core for rt2\n", SYSTEM_NAME_STRING_LOWER );
    ret = rts_isolator_run( reader_rt_func, -1, &g_cores_used[READ_THRD]);
    if( ret != ISOLATOR_OK)
    {
        isolator_lookup_error_msg(ret, error_msg);
        RTSLOG_INFO("Error : rts-cpu-isolator : %s\n", error_msg);
        stop_rt_thread(WRITE_THRD);
	    finish_dolphin();
        return -1;
    }

    return 0;
}

/// Kernel module cleanup function
void rt_cleanup( void )
{

    stop_rt_thread(WRITE_THRD);
    stop_rt_thread(READ_THRD);


    finish_dolphin();

    if ( WRITE_TO_FILE == 1) 
        write_data_to_file();

}

module_init( rt_init );
module_exit( rt_cleanup );



