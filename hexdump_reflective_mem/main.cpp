



#include <unistd.h>
#include <malloc.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include "sisci_types.h"
#include "sisci_api.h"
#include "sisci_error.h"


#define NO_CALLBACK                         NULL
#define NO_FLAGS                            0
//#define MIN_SEGMENT_SIZE                    16777216u
#define MIN_SEGMENT_SIZE                    4096u


static int                     verbose           = 0;
static sci_desc_t              sd;
static sci_error_t             error;
static sci_local_segment_t     localSegment;
static sci_map_t               localMap;
static sci_map_t               remoteMap;
static sci_remote_segment_t    remoteSegment;
static size_t                  offset            = 0;
static size_t                  segmentSize       = MIN_SEGMENT_SIZE;
static unsigned int            localAdapterNo    = 0;
static unsigned int            localNodeId       = 0;
static unsigned int            remoteNodeId      = 0;
static unsigned int            segmentId         = 0;



// Copyright for hexDump function
//-------------------------------------------------------------
//Copyright 2018 Dominik Liebler
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//
void hexDump(char *desc, volatile void *addr, int len)
{
    int i;
    unsigned char buff[17];
    unsigned char *pc = (unsigned char*)addr;

    // Output description if given.
    if (desc != NULL)
        printf ("%s:\n", desc);

    // Process every byte in the data.
    for (i = 0; i < len; i++) {
        // Multiple of 16 means new line (with line offset).

        if ((i % 16) == 0) {
            // Just don't print ASCII for the zeroth line.
            if (i != 0)
                printf("  %s\n", buff);

            // Output the offset.
            printf("  %04x ", i);
        }

        // Now the hex code for the specific character.
        printf(" %02x", pc[i]);

        // And store a printable ASCII character for later.
        if ((pc[i] < 0x20) || (pc[i] > 0x7e)) {
            buff[i % 16] = '.';
        } else {
            buff[i % 16] = pc[i];
        }

        buff[(i % 16) + 1] = '\0';
    }

    // Pad out last line if not exactly 16 characters.
    while ((i % 16) != 0) {
        printf("   ");
        i++;
    }

    // And print the final ASCII bit.
    printf("  %s\n", buff);
}


static void SleepMilliseconds(int milliseconds)
{

    if (milliseconds<1000) {
        usleep(1000*milliseconds);
    } else if (milliseconds%1000 == 0) {
        sleep(milliseconds/1000);
    } else {
        usleep(1000*(milliseconds%1000));
        sleep(milliseconds/1000);
    }

}



static sci_error_t create_and_test_reflective_memory()
{
    volatile uint8_t *readAddr; /* Read from reflective memory */
    volatile uint8_t *writeAddr; /* Write to reflective memory */
    unsigned int          value;
    unsigned int          written_value = 0;
    sci_sequence_t        sequence   = NULL;
    sci_sequence_status_t sequenceStatus;
    int                   node_offset;


    /* 
     * The segmentId refers to the reflective group id (0..max configured)
     * All nodes within the broadcast group must have the same segmentId.
     */

    /* Create local reflective memory segment */
    SCICreateSegment(sd,&localSegment,segmentId, segmentSize, NO_CALLBACK, NULL, SCI_FLAG_BROADCAST, &error);

    if (error == SCI_ERR_OK) {
        printf("Local segment (id=0x%x, size=%llu) is created.\n", segmentId,
                (unsigned long long) segmentSize);
    } else {
        fprintf(stderr,"SCICreateSegment failed: %s (0x%x)\n",
                SCIGetErrorString(error), error);
        return error;
    }

    /* Prepare the segment */

    SCIPrepareSegment(localSegment,localAdapterNo, NO_FLAGS, &error); 
    
    if (error == SCI_ERR_OK) {
        printf("Local segment (id=0x%x, size=%llu) is prepared.\n", segmentId,
                (unsigned long long) segmentSize);
    } else {
        fprintf(stderr,"SCIPrepareSegment failed: %s (0x%x)\n",
                SCIGetErrorString(error), error);
        return error;
    }


    /* Map local segment to user space - this is the address to read back data from the reflective memory region */
    readAddr = (volatile uint8_t *)SCIMapLocalSegment(localSegment,&localMap, offset,segmentSize, NULL,NO_FLAGS,&error);
    if (error == SCI_ERR_OK) {
        printf("Local segment (id=0x%x) is mapped to user space.\n", segmentId); 
    } else {
        fprintf(stderr,"SCIMapLocalSegment failed: %s (0x%x)\n",
                SCIGetErrorString(error), error);
        return error;
    } 


    /* Set the segment available */
    SCISetSegmentAvailable(localSegment, localAdapterNo, NO_FLAGS, &error);
    if (error == SCI_ERR_OK) {
        printf("Local segment (id=0x%x) is available for remote connections. \n", segmentId); 
    } else {
        fprintf(stderr,"SCISetSegmentAvailable failed: %s (0x%x)\n",
                SCIGetErrorString(error), error);
        return error;
    } 


    /* Connect to remote segment */
    printf("Connect to remote segment .... ");

    do { 
        SCIConnectSegment(sd,
                          &remoteSegment,
                          remoteNodeId,
                          segmentId,
                          localAdapterNo,
                          NO_CALLBACK,
                          NULL,
                          SCI_INFINITE_TIMEOUT,
                          SCI_FLAG_BROADCAST,
                          &error);

        SleepMilliseconds(10);

    } while (error != SCI_ERR_OK);

    printf("Remote segment (id=0x%x) is connected.\n", segmentId);


    /* Map remote segment to user space */
    writeAddr = (volatile uint8_t *)SCIMapRemoteSegment(remoteSegment,&remoteMap,offset,segmentSize,NULL,NO_FLAGS,&error);
    if (error == SCI_ERR_OK) {
        printf("Remote segment (id=0x%x) is mapped to user space. \n", segmentId);         
    } else {
        fprintf(stderr,"SCIMapRemoteSegment failed: %s (0x%x)\n",
                SCIGetErrorString(error), error);
        return error;
    } 


    //printf("Waiting for the writer to finish...\n");
    //SleepMilliseconds(10000);
    //printf("Done sleeping...\n");
	
    while( getchar() != 'q' )
    {

    	char dsc[] = "Dolphin reflective memory segment data:";
    	hexDump(dsc, readAddr, segmentSize);
        printf("\n\n");	
    }


    /* Lets clean up */
    /* Unmap local segment */
    SCIUnmapSegment(localMap,NO_FLAGS,&error);
    
    if (error == SCI_ERR_OK) {
        printf("The local segment is unmapped\n"); 
    } else {
        fprintf(stderr,"SCIUnmapSegment failed: %s (0x%x)\n",
                SCIGetErrorString(error), error);
        return error;
    }
    
    
    /* Remove local segment */
    SCIRemoveSegment(localSegment,NO_FLAGS,&error);
    if (error == SCI_ERR_OK) {
        printf("The local segment is removed\n"); 
    } else {
        fprintf(stderr,"SCIRemoveSegment failed: %s (0x%x)\n",
                SCIGetErrorString(error), error);
        return error;
    } 
    
    /* Unmap remote segment */
    SCIUnmapSegment(remoteMap,NO_FLAGS,&error);
    if (error == SCI_ERR_OK) {
        printf("The remote segment is unmapped\n"); 
    } else {
        fprintf(stderr,"SCIUnmapSegment failed: %s (0x%x)\n",
                SCIGetErrorString(error), error);
        return error;
    }
    
    /* Disconnect segment */
    SCIDisconnectSegment(remoteSegment,NO_FLAGS,&error);
    if (error == SCI_ERR_OK) {
        printf("The segment is disconnected\n"); 
    } else {
        fprintf(stderr,"SCIDisconnectSegment failed: %s (0x%x)\n",
                SCIGetErrorString(error), error);
        return error;
    } 
    
    
    return SCI_ERR_OK;
}



/*********************************************************************************/
/*                                M A I N                                        */
/*                                                                               */
/*********************************************************************************/


static sci_error_t QueryMulticastSupport(unsigned int  localAdapterNo,
                                         unsigned int *mcast_supported)
{
    sci_query_adapter_t queryAdapter = {0};
    sci_error_t         error;
    unsigned int        mcast_max_groups = 0;

    queryAdapter.localAdapterNo = localAdapterNo;
    queryAdapter.subcommand     = SCI_Q_ADAPTER_MCAST_MAX_GROUPS;
    queryAdapter.data           = &mcast_max_groups;

    SCIQuery(SCI_Q_ADAPTER, &queryAdapter, NO_FLAGS, &error);
    if (error != SCI_ERR_OK) {
        if (verbose) {
            fprintf(stderr,"SCIQuery() - SCI_Q_ADAPTER_MCAST_MAX_GROUPS failed\n");
        }
    }

    /* Multicast support is present if the number of multicast groups is not zero */
    *mcast_supported = mcast_max_groups;
    printf("Local adapter supports %u multicast groups\n\n", mcast_max_groups);

    return error;
}


int main(int argc, char *argv[])
{

    int counter;
    unsigned int MaxGroups;
    unsigned int MaxGroupID = 0;

    printf("%s compiled %s : %s\n\n",argv[0],__DATE__,__TIME__);

    /* Initialize the SISCI library */
    SCIInitialize(NO_FLAGS, &error);
    if (error != SCI_ERR_OK) {
        fprintf(stderr,"SCIInitialize failed: %s (0x%x)\n",
                SCIGetErrorString(error), error);
        return(-1);
    }

    /* Check if multicast support is present and how many groups*/
    error = QueryMulticastSupport(localAdapterNo, &MaxGroups);
    if (error != SCI_ERR_OK) {
        fprintf(stderr,"Failed to query multicast support: %s (0x%x)\n",
                SCIGetErrorString(error), error);
        SCITerminate();
        return error;
    }

    if (MaxGroups == 0) {
        printf("Reflective memory is not supported by the adapter type or in the current topology.\n"
               "Aborting test\n");
        SCITerminate();
        return 0;
    }
    MaxGroupID = MaxGroups - 1;



    /* Open a file descriptor */
    SCIOpen(&sd,NO_FLAGS,&error);
    if (error != SCI_ERR_OK) {
        if (error == SCI_ERR_INCONSISTENT_VERSIONS) {
            fprintf(stderr,"Version mismatch between SISCI user library and SISCI driver\n");
        }
        fprintf(stderr,"SCIOpen failed: %s (0x%x)\n",
                SCIGetErrorString(error), error);
        SCITerminate();
        return(-1); 
    }

    /* Get local nodeId */
    SCIGetLocalNodeId(localAdapterNo,
                      &localNodeId,
                      NO_FLAGS,
                      &error);

    if (error != SCI_ERR_OK) {
        fprintf(stderr,"Could not find the local adapter %d: %s (0x%x)\n",
                localAdapterNo, SCIGetErrorString(error), error);
        SCIClose(sd,NO_FLAGS,&error);
        SCITerminate();
        return(-1);
    }

    /*
     * Set remote nodeId to BROADCAST NODEID
     */
    remoteNodeId = DIS_BROADCAST_NODEID_GROUP_ALL;
    


    error = create_and_test_reflective_memory();
    if (error != SCI_ERR_OK) {
        return -1;
    }

    /* Close the file descriptor */
    SCIClose(sd,NO_FLAGS,&error);
    if (error != SCI_ERR_OK) {
        fprintf(stderr,"SCIClose failed: %s (0x%x)\n",
                SCIGetErrorString(error), error);
        SCITerminate();
        return(-1);
    }


    /* Free allocated resources */
    SCITerminate();

    return SCI_ERR_OK;
}



