///	@file module.c 
///	@brief 

#include <linux/version.h>
#include <linux/kernel.h> 
#include <linux/module.h>
#include <linux/cpu.h>
#include <linux/delay.h>
#include <net/sock.h>
#include <linux/netlink.h>
#include <linux/skbuff.h>
#include <asm/tsc.h> //cpu_khz

#include <asm/uaccess.h>
#include <asm/cacheflush.h>
#include <linux/ctype.h>
#include <linux/spinlock_types.h>

#include "common_utils.h"
#include "dolphin_netlink.h"
//#include "hist.h"
//#include "mstore.h"



#define RTS_LOG_PREFIX "dolphin_connect_segment"
#include "drv/rts-logger.h"




MODULE_DESCRIPTION( "" );
MODULE_AUTHOR( "LIGO" );
MODULE_LICENSE( "Dual BSD/GPL" );



#define DOLPHIN_BUF_SZ 16777216 // 16MB
#define MAX_DOLPHIN_INDEX 500000


// Start Module Parameters
//
static int ADAPTER_NUM = 0;
module_param(ADAPTER_NUM, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(ADAPTER_NUM, "The adapter we should use");

static int SEGMENT_ID = 0;
module_param(SEGMENT_ID, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(SEGMENT_ID, "The Dolphin segment ID we should use");


//
// Start Module Specific defines and vars
//
#define SYSTEM_NAME_STRING_LOWER "bandwidth_test" 
#define BILLION 1000000000ull
#define CPURATE_HZ (cpu_khz * 1000) 



// MAIN routine: Code starting point
// ****************************************************************
/// Startup function for initialization of kernel module.
int rt_init( void )
{
    unsigned segment_ids[] = {SEGMENT_ID};
    unsigned adapters[] = {ADAPTER_NUM};

    //Create 2 segments
    if (init_dolphin(1, DOLPHIN_BUF_SZ, segment_ids, adapters) == -1)
    {
        finish_dolphin();
        return -1;
    }

    return 0;
}

/// Kernel module cleanup function
void rt_cleanup( void )
{

    finish_dolphin();
}

module_init( rt_init );
module_exit( rt_cleanup );



