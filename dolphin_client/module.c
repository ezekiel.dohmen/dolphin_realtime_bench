/// @file module.c 
/// @brief 

#include <linux/version.h>
#include <linux/kernel.h> 
#include <linux/module.h>
#include <linux/cpu.h>
#include <linux/delay.h>
#include <net/sock.h>
#include <linux/netlink.h>
#include <linux/skbuff.h>

#include <asm/uaccess.h>
#include <asm/cacheflush.h>
#include <linux/ctype.h>
#include <linux/spinlock_types.h>

#include "rts-cpu-isolator.h" 

#define RTS_LOG_PREFIX "dolphin_client" 
#include "drv/rts-logger.h"

#include "hist.h"
#include "mstore.h"
#include "dolphin_netlink.h"

MODULE_DESCRIPTION( "" );
MODULE_AUTHOR( "LIGO" );
MODULE_LICENSE( "Dual BSD/GPL" );

static atomic_t g_atom_should_exit = ATOMIC_INIT(0); 
static atomic_t g_atom_has_exited = ATOMIC_INIT(0);

volatile uint64_t * g_read_addr = 0;
volatile uint64_t * g_write_addr = 0;

//atomic_t g_dolphin_init_success = ATOMIC_INIT(0);
int g_core_used;

//
// Start Module Parameters
//
static int ADAPTER_NUM = 0;
module_param(ADAPTER_NUM, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(ADAPTER_NUM, "The adapter we should use");

static int SEGMENT_ID = 0;
module_param(SEGMENT_ID, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(SEGMENT_ID, "The segment ID we should use");

static int NUM_COUNTERS_OPEN = 1;
module_param(NUM_COUNTERS_OPEN, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(NUM_COUNTERS_OPEN, "This parameter controls the number of counters the server/client should attempt to keep open simultaneously.");


static int CFLUSH_INDIVIDUALLY = 1;
module_param(CFLUSH_INDIVIDUALLY, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(CFLUSH_INDIVIDUALLY, "If 1 we cflush every write, else we cflush once per NUM_COUNTERS_OPEN");

static int MAX_DELAY_CUTOFF_NS = 50000;
module_param(MAX_DELAY_CUTOFF_NS, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(MAX_DELAY_CUTOFF_NS, "If a RTT is over this value, we will keep extra info on it.");


//
// Start Module Specific defines and vars
//
#define SYSTEM_NAME_STRING_LOWER "dolphin_client" 
#define BILLION 1000000000ull

#define DOLPHIN_BUF_SZ (NUM_COUNTERS_OPEN * sizeof(uint64_t))

uint64_t * g_start_tscs = NULL;

void rt_func( void )
{
    uint64_t count = 1, start_delay = 0,  total_sent = 0;
    uint64_t end_ns,  func_start_time, function_end_ns;
    int i =0;

    uint64_t min = 999999999, max = 0, avg = 0;
    mstore_context_t * mstore_ctx;
    hist_context_t * hist_ctx;
   
    //int64_t ranges [] = {2000, 2500, 3000, 3500, 4000, 5000, 6000, 7000, 8000, 9000, 10000,
    //                     11000, 12000, 13000, 14000, 15000, 16000, 17000,
    //                     18000, 20000, 22000, 25000, 30000};



    int64_t ranges [] = { 36000, 38000, 40000, 42000, 44000, 46000, 48000, 50000, 55000, 
                          60000, 65000, 70000, 75000, 80000, 85000, 90000, 100000};


    typedef struct over_thresh_event_t
    {
        uint64_t dur_ns;
        uint64_t since_start_us;
    } over_thresh_event_t;

    over_thresh_event_t over_event;

    mstore_ctx = initialize_mstore(sizeof(over_thresh_event_t), 1000);
    hist_ctx = hist_initialize(sizeof(ranges)/sizeof(ranges[0]), ranges);

    if( mstore_ctx == 0 || hist_ctx == 0)
    {
        atomic_set(&g_atom_has_exited, 1);
        return;
    }


    //Wait 10 s for RT timing to stabilize 
    while(atomic_read(&g_atom_should_exit) == 0 && start_delay < 5000000)
    {
        udelay(2);
        ++start_delay;
    }

    //Start the total time tracker
    timer_start(&func_start_time);

    while(atomic_read(&g_atom_should_exit) == 0)
    {


        for( int counter_indx=0; counter_indx < NUM_COUNTERS_OPEN; ++counter_indx)
        {

            //Start timer and write counter into reflective memory buffer
            timer_start(&g_start_tscs[counter_indx]);
            g_write_addr[counter_indx] = count;
            if( CFLUSH_INDIVIDUALLY ) {
                clflush_cache_range( (void*) &g_write_addr[counter_indx], sizeof(uint64_t) );
            }
        }
        if( !CFLUSH_INDIVIDUALLY ){
            clflush_cache_range( (void*) &g_write_addr[NUM_COUNTERS_OPEN-1], sizeof(uint64_t) );
        }

        ++count;

        for( int counter_indx=0; counter_indx < NUM_COUNTERS_OPEN; ++counter_indx)
        {

            unsigned num_waits_for_counter = 0;
            while( atomic_read(&g_atom_should_exit) == 0 && g_read_addr[counter_indx] != count )
            {
                //udelay(1);
                ndelay(250);
		++num_waits_for_counter;
		if (num_waits_for_counter == 4000000) {
                    RTSLOG_WARN("Have not gotten counter in over a second...\n");
		}
            }
            end_ns = timer_end_ns(&g_start_tscs[counter_indx]);


            over_event.dur_ns = end_ns ;
            over_event.since_start_us = timer_tock_ns(&func_start_time) / 1000;
            

            if( g_read_addr[counter_indx] == count )
            {
                if( over_event.dur_ns < min ) min = over_event.dur_ns;
                if( over_event.dur_ns > max ) max = over_event.dur_ns;
                avg += over_event.dur_ns;
                hist_add_element(hist_ctx, over_event.dur_ns);

                if(over_event.dur_ns > MAX_DELAY_CUTOFF_NS)
                {
                    RTSLOG_INFO("Got long RTT of %llu\n", over_event.dur_ns);
                    add_element(mstore_ctx, &over_event);
                }
                ++total_sent;

		if( total_sent % 222220 == 0 )
		{
		//RTSLOG_INFO("Last count was %llu\n", total_sent);
		}

            }
            else if (atomic_read(&g_atom_should_exit) == 0) {

                RTSLOG_ERROR("Bad count value returned, expected %lu, got %lu\n", count, g_read_addr[counter_indx]);
            }
        }

    } //while( keep running )

    function_end_ns = timer_end_ns(&func_start_time);

    RTSLOG_INFO("Count was %llu,  total time (s): %llu\n", total_sent,  function_end_ns / 1000000000);
    if( (function_end_ns/1000) >= 1) {
        RTSLOG_INFO("Estimated bandwidth (MB/s): %lu\n", (sizeof(uint64_t) * total_sent)/ (function_end_ns/1000) );
    }
    RTSLOG_INFO("Histogram Of All Latencies (ns)\n");
    hist_print_stats(hist_ctx);

    over_thresh_event_t * event_ptr;
    RTSLOG_INFO("Any latency over %u ns: ", MAX_DELAY_CUTOFF_NS);
    for(i = 0; i < get_num_elements(mstore_ctx); ++i)
    {
        event_ptr = get_element_ptr(mstore_ctx, i);
        RTSLOG_INFO("RTT: %lld, at offset (us) %llu\n", event_ptr->dur_ns, event_ptr->since_start_us);
    }

    hist_free(hist_ctx);
    free_mstore(mstore_ctx);

    // System reset command received
    atomic_set(&g_atom_has_exited, 1);
    return;
}



// MAIN routine: Code starting point
// ****************************************************************
/// Startup function for initialization of kernel module.
int rt_init( void )
{
    ISOLATOR_ERROR ret;
    char error_msg[ISOLATOR_ERROR_MSG_ALLOC_LEN];
    RTSLOG_INFO("%s: Locking free CPU core\n", SYSTEM_NAME_STRING_LOWER );
    unsigned segment_ids[] = {SEGMENT_ID};
    unsigned adapters[] = {ADAPTER_NUM};

    //Allocate dynamic globals
    g_start_tscs = kzalloc( sizeof(uint64_t) * NUM_COUNTERS_OPEN, GFP_KERNEL);
    if ( !g_start_tscs ) {
        printk("Error : Could not allocate %ld bytes for start times\n", sizeof(uint64_t) * NUM_COUNTERS_OPEN);
        return -1;
    }

    if (init_dolphin(1, DOLPHIN_BUF_SZ, segment_ids, adapters) == -1)
    {
        finish_dolphin();
        return -1;
    }
    g_read_addr = g_read_addrs[0];
    g_write_addr = g_write_addrs[0];

    ret = rts_isolator_run( rt_func, -1, &g_core_used);
    if( ret != ISOLATOR_OK)
    {
        isolator_lookup_error_msg(ret, error_msg);
        RTSLOG_INFO("Error : rts-cpu-isolator : %s\n", error_msg);
        finish_dolphin();
        return -1;
    }

    return 0;
}

/// Kernel module cleanup function
void rt_cleanup( void )
{
    ISOLATOR_ERROR ret;
    char error_msg[ISOLATOR_ERROR_MSG_ALLOC_LEN];
    uint64_t stop_sig_time_ns = get_monotonic_time_ns();

    // Stop the code and wait
    atomic_set(&g_atom_should_exit, 1);
    while (atomic_read(&g_atom_has_exited) == 0)
    {
        msleep( 1 );
    }

    RTSLOG_INFO("It took %lld ms for the RT code to exit.\n",
                 (get_monotonic_time_ns() - stop_sig_time_ns)/1000000);
    
    //At this point the CPU should be down...
    ret = rts_isolator_cleanup( g_core_used );
    if( ret != ISOLATOR_OK)
    {
        isolator_lookup_error_msg(ret, error_msg);
        RTSLOG_INFO("Error : rts-cpu-isolator : There was an error when calling rts_isolator_cleanup(): %s\n", error_msg);
    }


    finish_dolphin();

    //Free dynamic globals
    kfree(g_start_tscs);



}

module_init( rt_init );
module_exit( rt_cleanup );



