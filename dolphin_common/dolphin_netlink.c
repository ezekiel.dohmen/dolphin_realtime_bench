//
// This file contains the dolphin initialization/free functions that
// use netlink messages to request segments be allocated for its use.
// The dolphin_daemon must be runnign to handle the messages sent here.
//
//
//
//



#define RTS_LOG_PREFIX "dolphin_bw_bench"
#include "drv/rts-logger.h"

//LIGO Netlink Message Definitions
#include "daemon_messages.h"


//Netlink Socket Headers
#include <net/sock.h>
#include <linux/module.h>
#include <linux/netlink.h>
#include <linux/skbuff.h>

//Globals
static volatile unsigned g_requested_addrs = 0;
atomic_t g_dolphin_init_success = ATOMIC_INIT(0); 
static struct sock * g_nl_sock = NULL;

volatile void * g_read_addrs[8] = {0,};
volatile void * g_write_addrs[8] = {0,};

//
// Callback for the netlink unicast response message
//
static void netlink_recv_resp(struct sk_buff *skb)
{
    struct nlmsghdr *nlh;
    int pid;
    all_dolphin_daemon_msgs_union any_msg;

    //Store info and pointer to message
    nlh = (struct nlmsghdr *)skb->data;
    pid = nlh->nlmsg_pid; // pid of sending process
    any_msg.as_hdr = (dolphin_mc_header *) nlmsg_data(nlh);


    //Make sure we got (at least) a good header, before we dive into message
    if ( !check_mc_header_valid(any_msg.as_hdr, nlmsg_len(nlh)) )
    {
        RTSLOG_INFO("netlink_test_recv_msg() - Malformed message, too short or bad preamble\n");
        atomic_set(&g_dolphin_init_success, -1);
        return;
    }

    if(any_msg.as_hdr->interface_id != RT_KM_INTERFACE_ID)
    {
        RTSLOG_INFO("netlink_test_recv_msg() - Got a message that had an interface_id for somthing else.\n");
        atomic_set(&g_dolphin_init_success, -1);
        return; //Msg not ment for us
    }

    switch(any_msg.as_hdr->msg_id)
    {
        case DOLPHIN_DAEMON_ALLOC_RESP:
            if ( !check_alloc_resp_valid(any_msg.as_alloc_resp, nlmsg_len(nlh)) )
            {
                RTSLOG_INFO("netlink_test_recv_msg() - Got a DOLPHIN_DAEMON_ALLOC_RESP, but message was not valid, discarding. "
                        "Size was %u\n", nlmsg_len(nlh));
                atomic_set(&g_dolphin_init_success, -1);
                return;
            }

            if ( any_msg.as_alloc_resp->status != DOLPHIN_ERROR_OK )
            {
                RTSLOG_INFO("netlink_test_recv_msg() - An error (%d) was returned by the dolphin daemon. Failed to init dolphin.\n", any_msg.as_alloc_resp->status);
                atomic_set(&g_dolphin_init_success, -1);
                return;
            }

            for(int i=0; i < any_msg.as_alloc_resp->num_addrs; ++i)
            {
                //Store pointers for use later, int div on the right
                g_read_addrs[i] = (volatile unsigned long*)any_msg.as_alloc_resp->addrs[ i ].read_addr;
                g_write_addrs[i] = (volatile unsigned long*)any_msg.as_alloc_resp->addrs[ i ].write_addr;
            }
        break;

        default:
            RTSLOG_INFO("netlink_test_recv_msg() - Got a message with ID %u, which we don't support, discarding.\n", any_msg.as_hdr->msg_id);
            atomic_set(&g_dolphin_init_success, -1);
            return;
        break;

    }
    atomic_set(&g_dolphin_init_success, 1);//Signal init is done

}

static struct netlink_kernel_cfg g_netlink_cfg = {
    .input = netlink_recv_resp,
};



int init_dolphin( unsigned num_segments, unsigned segment_sz, unsigned segment_ids[], unsigned adapters_nums[] )
{
    struct sk_buff *skb_out;
    struct nlmsghdr *nlh;
    int res;
    dolphin_mc_alloc_req * alloc_req;


    unsigned total_payload_sz = GET_ALLOC_REQ_SZ(num_segments);

 

    g_nl_sock = netlink_kernel_create(&init_net, DOLPHIN_DAEMON_REQ_LINK_ID, &g_netlink_cfg);
    if (!g_nl_sock) {
        RTSLOG_INFO("init_dolphin() - Error creating netlink socket.\n");
        return -1;
    }



    //Send one request
    skb_out = nlmsg_new( total_payload_sz, 0);
    if (!skb_out) {
        RTSLOG_INFO(KERN_ERR "netlink_test: Failed to allocate new skb\n");
        return -1;
    }

    //Allocate and send message
    nlh = nlmsg_put(skb_out, 0, 0, NLMSG_DONE, total_payload_sz, 0);


    alloc_req = (dolphin_mc_alloc_req*) nlmsg_data(nlh);
    alloc_req->header.msg_id = DOLPHIN_DAEMON_ALLOC_REQ;
    alloc_req->header.preamble_seq = DOLPHIN_DAEMON_MSG_PREAMBLE;
    alloc_req->num_segments = num_segments;
    for(int i =0; i < num_segments; ++i)
    {
        alloc_req->segments[i].segment_id = segment_ids[i];
        alloc_req->segments[i].adapter_num = adapters_nums[i];
        alloc_req->segments[i].segment_sz_bytes = segment_sz;

    }

    res = nlmsg_multicast(g_nl_sock, skb_out, 0, DOLPHIN_DAEMON_REQ_GRP_ID, GFP_KERNEL);
    skb_out = NULL;


    //We wait for response to be handled by netlink_recv_resp()
    int num_waits = 100;
    int count = 0;
    while( atomic_read(&g_dolphin_init_success) == 0 && count < num_waits)
    {
        msleep( 20 );
        ++count;
    }

    //Timeout case
    if(atomic_read(&g_dolphin_init_success) == 0)
    {
        RTSLOG_INFO("init_dolphin() - Did not get a response from the dolphin_daemon in a timely manner.");
        return -1;
    }

    //If there was a returned error
    if(atomic_read(&g_dolphin_init_success) < 0)
    {
        return -1;
    }

    //Otherwise globals are set and we should be good to go

    //Clean up the netlink socket, so other modules can create one and talk to the deamon
    netlink_kernel_release(g_nl_sock);
    g_nl_sock = NULL;

    return 0;


}

void finish_dolphin( void )
{
    struct sk_buff *skb_out;
    struct nlmsghdr *nlh;
    int res;
    dolphin_mc_free_all_req * free_req;

    unsigned total_payload_sz = sizeof( dolphin_mc_free_all_req );

    g_nl_sock = netlink_kernel_create(&init_net, DOLPHIN_DAEMON_REQ_LINK_ID, &g_netlink_cfg);
    if (!g_nl_sock) {
        RTSLOG_INFO("finish_dolphin() - Error creating netlink socket.\n");
        return;
    }

    //if( g_nl_sock == NULL) return; //If we never set up the socket, just return

    //Allocate the request's msg space, OS does free once it is sent
    skb_out = nlmsg_new( total_payload_sz, 0);
    if (!skb_out) {
        RTSLOG_INFO("Failed to allocate skb for free request. Not sending...\n");
        return;
    }

    //Allocate and send message
    nlh = nlmsg_put(skb_out, 0, 0, NLMSG_DONE, total_payload_sz, 0);


    free_req = (dolphin_mc_free_all_req*) nlmsg_data(nlh);
    free_req->header.msg_id = DOLPHIN_DAEMON_FREE_REQ;
    free_req->header.preamble_seq = DOLPHIN_DAEMON_MSG_PREAMBLE;

    res = nlmsg_multicast(g_nl_sock, skb_out, 0, DOLPHIN_DAEMON_REQ_GRP_ID, GFP_KERNEL);
    if( res != 0 )
        RTSLOG_INFO("finish_dolphin() - nlmsg_multicast() returned an error: %d\n", res);

    netlink_kernel_release(g_nl_sock);
    g_nl_sock = NULL;


    return;
}