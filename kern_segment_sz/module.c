///	@file module.c 
///	@brief 

#include <linux/version.h>
#include <linux/kernel.h> 
#include <linux/module.h>
#include <linux/cpu.h>
#include <linux/delay.h>
#include <net/sock.h>
#include <linux/netlink.h>
#include <linux/skbuff.h>
#include <asm/tsc.h> //cpu_khz

#include <asm/uaccess.h>
#include <asm/cacheflush.h>
#include <linux/ctype.h>
#include <linux/spinlock_types.h>

#include "common_utils.h"
#include "dolphin_netlink.h"
//#include "hist.h"
//#include "mstore.h"


#define RTS_LOG_PREFIX "dolphin_bw_bench"
#include "drv/rts-logger.h"




MODULE_DESCRIPTION( "" );
MODULE_AUTHOR( "LIGO" );
MODULE_LICENSE( "Dual BSD/GPL" );

#define NUM_RT_CORES 2
#define WRITE_THRD 0
#define READ_THRD 1


#define DOLPHIN_BUF_SZ 16777216 // 16MB
#define MAX_DOLPHIN_INDEX 500000


// Start Module Parameters
//
static int ADAPTER_NUM = 1;
module_param(ADAPTER_NUM, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(ADAPTER_NUM, "The adapter we should use");


//
// Start Module Specific defines and vars
//
#define SYSTEM_NAME_STRING_LOWER "kern_segment_sz_test" 



void sz_check( void )
{
    uint64_t counter = 0;
    volatile uint8_t * write_addr1 = (volatile uint8_t *)g_write_addrs[0];
    volatile uint8_t * read_addr1 = (volatile uint8_t *)g_read_addrs[0];
    volatile uint8_t * write_addr2 = (volatile uint8_t *)g_write_addrs[1];
    volatile uint8_t * read_addr2 = (volatile uint8_t *)g_read_addrs[1];


    if (write_addr1 == NULL || read_addr1 == NULL || write_addr2 == NULL || read_addr2 == NULL){
        // System reset command received
        RTSLOG_INFO("read_addr or write_addr is NULL, exiting...\n");
        return;
    }


    for(unsigned i =0; i < DOLPHIN_BUF_SZ; ++i)
    {
        write_addr1[i] = 5;
        ++counter;
        clflush_cache_range( (void*)&write_addr1[i], 1 );
    }


    RTSLOG_INFO("Done with test, counter is %llu\n", counter);

    return;
}




// MAIN routine: Code starting point
// ****************************************************************
/// Startup function for initialization of kernel module.
int rt_init( void )
{
    unsigned segment_ids[] = {0, 1};
    unsigned adapters[] = {ADAPTER_NUM, ADAPTER_NUM};

    //Create 2 segments
    if (init_dolphin(2, DOLPHIN_BUF_SZ, segment_ids, adapters) == -1)
    {
        finish_dolphin();
        return -1;
    }

    sz_check();

    return 0;
}

/// Kernel module cleanup function
void rt_cleanup( void )
{
    finish_dolphin();


}

module_init( rt_init );
module_exit( rt_cleanup );



