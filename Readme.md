# Overview
This repository holds Dolphin benchmarking applications that use the Dolphin Kernelspace (genif) interface. The modules use LIGOs [real time isolator](https://git.ligo.org/cds/software/advligorts/-/tree/master/src/drv/rts-cpu-isolator?ref_type=heads) to achieve deterministic timing. 

# Test Prerequisites
- LIGO Packages
    - advligorts-logger-dkms 
    - advligorts-cpu-isolator-dkms
    - advligorts-dolphin-daemon
    - advligorts-dolphin-proxy-km-dkms
- Dolphin drivers should also be installed/loaded, with adapters configured on server and client

## dolphin_server and dolphin_client test

### Test Steps
1. Build both modules 
2. Start the server module on one machine
3. Start client on the other
4. Wait for desired testing time 
5. Remove client then server modules
6. Check dmesg log for RTT results.