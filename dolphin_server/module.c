///	@file module.c 
///	@brief 

#include <linux/version.h>
#include <linux/kernel.h> 
#include <linux/module.h>
#include <linux/cpu.h>
#include <linux/delay.h>
#include <net/sock.h>
#include <linux/netlink.h>
#include <linux/skbuff.h>

#include <asm/uaccess.h>
#include <asm/cacheflush.h>
#include <linux/ctype.h>
#include <linux/spinlock_types.h>

#include "hist.h"
#include "mstore.h"
#include "dolphin_netlink.h"


#include "rts-cpu-isolator.h" 



MODULE_DESCRIPTION( "" );
MODULE_AUTHOR( "LIGO" );
MODULE_LICENSE( "Dual BSD/GPL" );

static atomic_t g_atom_should_exit = ATOMIC_INIT(0); 
static atomic_t g_atom_has_exited = ATOMIC_INIT(0);

volatile uint64_t * g_read_addr = 0;
volatile uint64_t * g_write_addr = 0;


//
// Start Module Parameters
//
static int ADAPTER_NUM = 0;
module_param(ADAPTER_NUM, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(ADAPTER_NUM, "The adapter we should use");

static int SEGMENT_ID = 0;
module_param(SEGMENT_ID, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(SEGMENT_ID, "The segment ID we should use");

static int NUM_COUNTERS_OPEN = 1;
module_param(NUM_COUNTERS_OPEN, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(NUM_COUNTERS_OPEN, "This parameter controls the number of counters the server/client should attempt to keep open simultaneously.");

static int g_core_used = -1;

//
// Start Module Specific defines and vars
//
#define SYSTEM_NAME_STRING_LOWER "dolphin_server" 
#define BILLION 1000000000ull
#define CPURATE_HZ (cpu_khz * 1000) //TODO: Where does cpu_khz come from?

#define DOLPHIN_BUF_SZ (NUM_COUNTERS_OPEN * sizeof(uint64_t))


uint64_t * start_tscs = NULL;


void rt_func( void )
{
    uint64_t count = 0, expected_val = 1, cur_val;
    uint64_t total_missed = 0, i =0;
    int64_t count_diff;
    uint64_t dur_ns;
    uint64_t min = 999999999, max = 0, avg = 0, max_index = 0;
    hist_context_t * hist_ctx;
    mstore_context_t * mstore_ctx;

    int64_t ranges [] = {2000, 2500, 3000, 3500, 4000, 5000, 6000, 7000, 8000, 9000, 10000, 
                         11000, 12000, 13000, 14000, 15000, 16000, 17000, 
    			 18000, 20000, 22000, 25000, 30000};

    //int64_t ranges [] = { 36000, 38000, 40000, 42000, 44000, 46000, 48000, 50000, 60000, 70000, 80000, 100000};

    hist_ctx = hist_initialize(sizeof(ranges)/sizeof(ranges[0]), ranges);
    mstore_ctx = initialize_mstore(sizeof(uint64_t), 100);

    while(atomic_read(&g_atom_should_exit) == 0)
    {

        for( int counter_indx=0; counter_indx < NUM_COUNTERS_OPEN; ++counter_indx)
        {

            //Wait for counter to be updated
            cur_val = g_read_addr[counter_indx];
            while( atomic_read(&g_atom_should_exit) == 0 && cur_val < expected_val )
            {
                udelay(1);
                cur_val = g_read_addr[counter_indx];
            }
            dur_ns = timer_end_ns(&start_tscs[counter_indx]);

            if(atomic_read(&g_atom_should_exit)) {
                break;
            }


            if(cur_val != 1)
            {

                if( dur_ns < min ) min = dur_ns;
                if( dur_ns > max )
                {
                    max = dur_ns;
                    max_index = count;
                }
                hist_add_element(hist_ctx, dur_ns);
                avg+= dur_ns;
            }


            if(cur_val != expected_val)
            {
                //We have a higher value than expected, so we missed (at least) one
                count_diff = cur_val - expected_val;
                total_missed += count_diff;
                expected_val = cur_val + 1;
                printk("AFTER: cur: %llu , ex: %llu\n", cur_val, expected_val);
                add_element(mstore_ctx, &count_diff);
            }

            
            ++count;

            g_write_addr[counter_indx] = expected_val+1;
            clflush_cache_range( (void*) &g_write_addr[counter_indx], sizeof(uint64_t) );
            timer_start(&start_tscs[counter_indx]);
            


        }//For each open counter
        ++expected_val;
        
    } //while( keep running )


    printk("Count was %llu, max percent in data %llu%%\n", count, (count) ? (max_index*100)/count : 0 );
    common_print("Histogram Of All Latencies (ns)\n");
    hist_print_stats(hist_ctx);

    common_print("Missed counters: ");
    for(i = 0; i < get_num_elements(mstore_ctx); ++i)
    {
        common_print("%lld\n", get_element_as_int64_t(mstore_ctx, i));
    }


    // System reset command received
    atomic_set(&g_atom_has_exited, 1);
    return;
}



// MAIN routine: Code starting point
// ****************************************************************
/// Startup function for initialization of kernel module.
int rt_init( void )
{
    ISOLATOR_ERROR ret;
    char error_msg[ISOLATOR_ERROR_MSG_ALLOC_LEN];
    unsigned segment_ids[] = {SEGMENT_ID};
    unsigned adapters[] = {ADAPTER_NUM};


    //Allocate dynamic globals
    start_tscs = kzalloc( sizeof(uint64_t) * NUM_COUNTERS_OPEN, GFP_KERNEL);
    if ( !start_tscs ) {
        printk("Error : Could not allocate %ld bytes for start times\n", sizeof(uint64_t) * NUM_COUNTERS_OPEN);
        return -1;
    }


    printk("%s: Locking free CPU core\n", SYSTEM_NAME_STRING_LOWER );

    if (init_dolphin(1, DOLPHIN_BUF_SZ, segment_ids, adapters) == -1)
    {
        finish_dolphin();
        return -1;
    }
    g_read_addr = g_read_addrs[0];
    g_write_addr = g_write_addrs[0];




    ret = rts_isolator_run( rt_func, -1, &g_core_used);
    if( ret != ISOLATOR_OK)
    {
        isolator_lookup_error_msg(ret, error_msg);
        printk("Error : rts-cpu-isolator : %s\n", error_msg);
	    finish_dolphin();
        return -1;
    }

    return 0;
}

/// Kernel module cleanup function
void rt_cleanup( void )
{
    ISOLATOR_ERROR ret;
    char error_msg[ISOLATOR_ERROR_MSG_ALLOC_LEN];
    uint64_t stop_sig_time_ns = get_monotonic_time_ns();

    // Stop the code and wait
    atomic_set(&g_atom_should_exit, 1);
    while (atomic_read(&g_atom_has_exited) == 0)
    {
        msleep( 1 );
    }

    printk("It took %lld ms for the RT code to exit.\n",
                 (get_monotonic_time_ns() - stop_sig_time_ns)/1000000);
    
    //At this point the CPU will be down...
    ret = rts_isolator_cleanup( g_core_used );
    if( ret != ISOLATOR_OK)
    {
        isolator_lookup_error_msg(ret, error_msg);
        printk("Error : rts-cpu-isolator : There was an error when calling rts_isolator_cleanup(): %s\n", error_msg);
    }



    finish_dolphin();

    //Free dynamic globals
    kfree(start_tscs);


}

module_init( rt_init );
module_exit( rt_cleanup );



