///	@file module.c 
///	@brief 

#include <linux/version.h>
#include <linux/kernel.h> 
#include <linux/module.h>
#include <linux/cpu.h>
#include <linux/delay.h>
#include <net/sock.h>
#include <linux/netlink.h>
#include <linux/skbuff.h>

#include <asm/uaccess.h>
#include <asm/cacheflush.h>
#include <linux/ctype.h>
#include <linux/spinlock_types.h>


#define RTS_LOG_PREFIX "dolphin_flush_only_test"
#include "drv/rts-logger.h"

//rts-cpu-isolator
#include "rts-cpu-isolator.h" 

//LIGO Netlink Message Definitions
#include "daemon_messages.h"

#include "hist.h"
//#include "mstore.h"
#include "dolphin_netlink.h"


MODULE_DESCRIPTION( "" );
MODULE_AUTHOR( "LIGO" );
MODULE_LICENSE( "Dual BSD/GPL" );

// Start Module Parameters
//
static int ADAPTER_NUM = 0;
module_param(ADAPTER_NUM, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(ADAPTER_NUM, "The adapter we should use");

int MP_CPU_ID = -1;
module_param(MP_CPU_ID, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(MP_CPU_ID, "The CPU core we should use, -1 means any");


static atomic_t g_atom_should_exit = ATOMIC_INIT(0); 
static atomic_t g_atom_has_exited = ATOMIC_INIT(0);
static int g_core_used;
volatile void * g_read_addr = 0;
volatile void * g_write_addr = 0;





//
// Start Module Specific defines and vars
//
#define SYSTEM_NAME_STRING_LOWER "dolphin_flush_only_test" 



void rt_func( void )
{
    uint64_t count = 1, start_delay = 0, err_cnt = 0;
    uint64_t start_ns, dur_ns;

    //mstore_context_t * mstore_ctx;
    hist_context_t * hist_ctx;
    int64_t ranges [] = {2000, 4000, 5000, 6000, 7000, 8000, 
	                 9000, 10000, 11000, 12000, 13000, 14000, 
			 15000, 16000, 17000, 18000, 20000, 22000, 25000, 30000};

    //mstore_ctx = initialize_mstore(sizeof(uint64_t), 100);
    hist_ctx = hist_initialize(sizeof(ranges)/sizeof(ranges[0]), ranges);

    
    while(atomic_read(&g_atom_should_exit) == 0 && start_delay < 5000000)
    {
	    udelay(2);
	    ++start_delay;
    }


    while(atomic_read(&g_atom_should_exit) == 0)
    {
        //*((uint64_t *)g_write_addr) = count;

        timer_start( &start_ns );
	    clflush_cache_range( (void*)g_write_addr, 16 );
        dur_ns = timer_end_ns( &start_ns );



        ++count;
        udelay(1);


        hist_add_element(hist_ctx, dur_ns);


    }


    RTSLOG_INFO("Count was %llu, err_cnt: %llu\n", count, err_cnt);
    common_print("Histogram Of All Latencies (ns)\n");
    hist_print_stats(hist_ctx);

    if(err_cnt > 0)
    {
        RTSLOG_INFO("BENCHMARK FAIL: error count is %llu\n", err_cnt);
        atomic_set(&g_atom_has_exited, 1);
        return;
    }




    // System reset command received
    atomic_set(&g_atom_has_exited, 1);
    return;
}




// MAIN routine: Code starting point
// ****************************************************************
/// Startup function for initialization of kernel module.
int rt_init( void )
{
    unsigned segment_ids[] = {3};
    unsigned adapters[] = {ADAPTER_NUM};
    ISOLATOR_ERROR ret;
    char error_msg[ISOLATOR_ERROR_MSG_ALLOC_LEN];


    if (init_dolphin(1, 8192, segment_ids, adapters) == -1)
    {
        return -1;
    }
    g_read_addr = g_read_addrs[0];
    g_write_addr = g_write_addrs[0];


    // The isolate the thread on the RT core
    RTSLOG_INFO("%s: Locking free CPU core\n", SYSTEM_NAME_STRING_LOWER );
    ret = rts_isolator_run( rt_func, MP_CPU_ID, &g_core_used);
    if( ret != ISOLATOR_OK)
    {
        isolator_lookup_error_msg(ret, error_msg);
        RTSLOG_INFO("Error : rts-cpu-isolator : %s\n", error_msg);
	    finish_dolphin();
        return -1;
    }
    return 0;
}

/// Kernel module cleanup function
void rt_cleanup( void )
{
    uint64_t stop_sig_time_ns = get_monotonic_time_ns();
    ISOLATOR_ERROR ret;
    char error_msg[ISOLATOR_ERROR_MSG_ALLOC_LEN];

    // Stop the code and wait
    atomic_set(&g_atom_should_exit, 1);
    while (atomic_read(&g_atom_has_exited) == 0)
    {
        msleep( 1 );
    }

    RTSLOG_INFO("It took %lld ms for the RT code to exit.\n",
                 (get_monotonic_time_ns() - stop_sig_time_ns)/1000000);
    
    ret = rts_isolator_cleanup( g_core_used );
    if( ret != ISOLATOR_OK)
    {
        isolator_lookup_error_msg(ret, error_msg);
        RTSLOG_INFO("Error : rts-cpu-isolator : There was an error when calling rts_isolator_cleanup(): %s\n", error_msg);
    }

    finish_dolphin();


}

module_init( rt_init );
module_exit( rt_cleanup );



