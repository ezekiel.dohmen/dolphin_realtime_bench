#ifndef LIGO_MSTORE_H
#define LIGO_MSTORE_H

#ifndef USERMODE_BUILD
#define KERNELMODE_BUILD
#endif

#include "common_utils.h"

#ifdef KERNELMODE_BUILD
#include <linux/string.h>
#else
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#endif


typedef struct mstore_context_t
{
    int num_ele;
    int size_of_ele;
    int cur_ele;
    int num_added;
    char * block;
} mstore_context_t;

mstore_context_t * initialize_mstore(int size_of_element_bytes, int total_elements)
{


    mstore_context_t * context = (mstore_context_t *) common_malloc(sizeof(mstore_context_t));
    if(context == NULL)
        return NULL;

    context->block = (char*) common_malloc(size_of_element_bytes * total_elements);
    if(context->block == NULL)
    {
        common_free(context);
        return NULL;
    }
    memset(context->block, 0, size_of_element_bytes * total_elements);

    context->num_ele = total_elements;
    context->size_of_ele = size_of_element_bytes;
    context->cur_ele = 0;
    context->num_added = 0;

    return context;
}

void add_element(mstore_context_t * context, void * new_ele)
{
    memcpy(&context->block[context->size_of_ele * context->cur_ele], new_ele, context->size_of_ele);
    context->cur_ele = (context->cur_ele + 1) % context->num_ele;
    if(context->num_added < context->num_ele)
        ++context->num_added;
    return;
}


void * get_element_ptr(mstore_context_t * context, int index)
{
    if(index > context->num_ele)
        return 0;

    return &context->block[index * context->size_of_ele];
}

int32_t get_element_as_int32_t (mstore_context_t * ctx, int index)
{
    return *((int32_t*)get_element_ptr(ctx, index));
}

uint32_t get_element_as_uint32_t (mstore_context_t * ctx, int index)
{
    return *((uint32_t*)get_element_ptr(ctx, index));
}

int64_t get_element_as_int64_t (mstore_context_t * ctx, int index)
{
    return *((int64_t*)get_element_ptr(ctx, index));
}

uint64_t get_element_as_uint64_t (mstore_context_t * ctx, int index)
{
    return *((uint64_t*)get_element_ptr(ctx, index));
}


int get_num_elements(mstore_context_t * context)
{
    return context->num_added;
}


void free_mstore(mstore_context_t * context)
{
    common_free(context->block);
    common_free(context);
    context = 0;
    return;
}

#endif


