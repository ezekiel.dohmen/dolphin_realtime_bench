#ifndef COMMON_UTILS_H
#define COMMON_UTILS_H



#ifdef __KERNEL__
#include <linux/slab.h>
#include <asm/msr.h> //rdtsc_ordered()
#include <asm/tsc.h> //tsc_khz

static inline void* common_malloc(uint64_t size)
{
    return kmalloc(size, GFP_NOWAIT);
}

static inline void common_free(void * ptr)
{
    kfree(ptr);
}

#define COMMON_CPURATE_KHZ ((uint64_t)(cpu_khz))

static inline uint64_t get_monotonic_time_ns( void )
{
    return (rdtsc() * 1000000 ) / COMMON_CPURATE_KHZ ; 
}

static inline void timer_start( uint64_t * start_tsc)
{
    *start_tsc = rdtsc_ordered();
}

static inline uint64_t timer_tock_ns( uint64_t * start_tsc)
{
    return ((rdtsc_ordered() - *start_tsc) * 1000000ULL ) / tsc_khz ;
}

static inline uint64_t timer_end_ns( uint64_t * start_tsc)
{
    *start_tsc = ((rdtsc_ordered() - *start_tsc) * 1000000ULL ) / tsc_khz ;
    return *start_tsc; //This is now the duration in ns
}


#define common_print printk

#else //User Mode
#include <stdlib.h>
#include <stdint.h>
#include <time.h>

static inline void* common_malloc(uint64_t size)
{
    return malloc(size);
}

static inline void common_free(void * ptr)
{
    free(ptr);
}

static inline uint64_t get_monotonic_time_ns( void )
{
    static struct timespec cur_time;
    clock_gettime( CLOCK_MONOTONIC, &cur_time );
    return cur_time.tv_sec*1e9 + cur_time.tv_nsec;;
}


#define common_print printf

#endif //ifdef __KERNEL__


#endif
