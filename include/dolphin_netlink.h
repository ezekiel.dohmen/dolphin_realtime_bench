#ifndef DOLPHIN_NETLINK_HH
#define DOLPHIN_NETLINK_HH

extern volatile void * g_read_addrs[8];
extern volatile void * g_write_addrs[8];

/*
    This function will attempt to init the requested dolphin segments witht he given config.

    The function expects num_segments number of variables pointed to by segment_ids and adapters_nums
*/
int init_dolphin( unsigned num_segments, unsigned segment_sz, unsigned segment_ids[], unsigned adapters_nums[] );
void finish_dolphin( void );


#endif //DOLPHIN_NETLINK_HH